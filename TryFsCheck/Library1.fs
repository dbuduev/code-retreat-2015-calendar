﻿namespace TryFsCheck

open NUnit.Framework
open FsCheck.NUnit

module Calendar =
   type Time = int
   type Appointment = Time * Time

   let startTime : Time = 0
   let endTime : Time = 24

   let normalize xs = xs |> Seq.filter (fun (s, e) -> s <> e) 

   let invert xs = 
      if Seq.isEmpty xs then seq [(startTime, endTime)]
      else 
         seq {
            yield (startTime, xs |> Seq.head |> fst)
            yield! (Seq.pairwise xs |> Seq.map (fun ((_, e), (s, _)) -> (e, s)))
            yield (xs |> Seq.last |> snd, endTime)
         } |> normalize

   let merge xs ys =
      seq {
         for (a,b) in xs do
            for (c,d) in ys |> Seq.skipWhile (fun (c,d) -> d <= a) |> Seq.takeWhile (fun(c, d) -> c < b) do
               yield ((max a c), (min b d))
      } 

   let find calendars =
      calendars |> Seq.map invert |> Seq.reduce merge |> Seq.tryFind (fun _ -> true)

open Calendar   
[<TestFixture>]
type Tests() =

   [<Test>]
   member this.``finds the inversion of a calendar``() =
      let input = [(0,1); (1,2); (3,4);(5,6)]
      let expected = [(2,3); (4,5); (6,24)]
      Assert.AreEqual(expected, invert input)
   
   [<Property(StartSize = 0, EndSize = 24)>]
   member this.``Invert invert gives the same``(xs:List<Appointment>) =
      xs = (xs |> invert |> Seq.toList |> invert |> Seq.toList)
   
   [<Test>]
   member this.``Merge intervals basic``() =
      Assert.AreEqual([(3,4)], merge [(2,4)] [(3,5)])

   [<Test>]
   member this.``Merge intervals basic. Gives no overlap``() =
      Assert.AreEqual([], merge [(2,3)] [(3,5)])

   [<Test>]
   member this.``Merge intervals several intervals.``() =
      Assert.AreEqual([(2,3);(5,6)], merge [(2, 3); (5, 7)] [(1,3);(4, 6);(7, 10)])

   [<Test>]
   member this.``Merge intervals several intervals 2.``() =
      Assert.AreEqual([(5,6)], merge [(2, 3); (5, 7)] [(1,2);(4, 6);(7, 10)])

   [<Test>]
   member this.``Find ref merge test basic.``() =
      Assert.AreEqual(Some (3,4), find [[(startTime,2);(4,endTime)];[(startTime,3);(5,endTime)]])